﻿using System.Windows.Forms;

namespace LearnSql
{
    //Интерфейс для раскраски синтаксиса RichTextBox
    interface IColorize
    {
        void Do(bool up, ref RichTextBox box);
    }
}
