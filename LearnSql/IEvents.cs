﻿using System;

namespace LearnSql
{
    //интерфейс для событий оповещения
    interface IEvents
    {
        event Action<string> OnError;
        event Action<string> OnSuccess;
    }
}
