﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace LearnSql
{
    public class SqlConnector : IConnector, ITableConnector, IEvents
    {
        //опрделение провайдера для фабрики провайдеров
        private readonly string _provider = ConfigurationManager.AppSettings["sqlProvider"];

        //строка подключения к БД определённая в App.config
        private readonly string _connectionString = ConfigurationManager.ConnectionStrings["AutoLotSqlProvider"]
            .ConnectionString;

        //адаптер для SQL БД
        private SqlDataAdapter _dAdapt;

        //инициируем событие при появлении перехваченного исключения
        public event Action<string> OnError;

        //инициируем событие для сообщений о выполнении
        public event Action<string> OnSuccess;

        public SqlConnector() => _dAdapt = new SqlDataAdapter();    

        //получаем конкретного провайдера из фабрики провайдеров
        public DbProviderFactory GetProvider(string dataProvider) => DbProviderFactories.GetFactory(dataProvider);

        //возвращаем подключение к БД с конкретным провайдером
        private DbConnection Connect()
        {
            DbConnection connection = GetProvider(_provider).CreateConnection();
            if (connection != null)
            {
                connection.ConnectionString = _connectionString;
                return connection;
            }
            OnError?.Invoke("Create Connection error");
            return null;
        }

        //конфигурируем адаптер для взаимодействия с dataGridView
        private void ConfigureAdapter(string query, out SqlDataAdapter dAdapt)
        {
            dAdapt = new SqlDataAdapter(query, _connectionString);
            //автоматически заполняем команды для CRUD операций
            SqlCommandBuilder builder = new SqlCommandBuilder(dAdapt);
        }

        //отправляем запрос к БД
        public void DoQuery(string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                DbConnection connection = Connect();
                //создаём новую команду
                DbCommand command = new SqlCommand(query, (SqlConnection) connection);

                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    OnSuccess?.Invoke("Command Success!");
                }
                catch (SqlException ex)
                {
                    OnError?.Invoke(ex.ToString());
                }
                catch (Exception ex)
                {
                    OnError?.Invoke(ex.ToString());
                }
                finally
                {
                    if (connection.State == ConnectionState.Open) connection.Close();
                }
            }
            OnSuccess?.Invoke("Query is empty or Incorrect");
        }

        //получаем все таблицы в БД кроме системной sysdiagrams
        public string[] GetTables()
        {
            DbConnection connection = Connect();
            List<string> tables = new List<string>();

            try
            {
                connection.Open();
                DataTable table = connection.GetSchema("Tables");

                foreach (DataRow row in table.Rows)
                {
                    string tablename = (string) row[2];
                    if (tablename != "sysdiagrams") tables.Add(tablename);
                }

                OnSuccess?.Invoke("Get table names");
                return tables.ToArray();
            }
            catch (Exception ex)
            {
                OnError?.Invoke(ex.ToString());
            }
            finally
            {
                if (connection.State == ConnectionState.Open) connection.Close();
            }

            OnSuccess?.Invoke("No tables in Data Base");
            return null;
        }

        //получаем данные для вставки в dataGridView
        public DataTable GetTableData(string tableName, string query = null)
        {
            //конфигурируем стандартную команду если не была задана другая
            if (string.IsNullOrEmpty(query)) query = $"SELECT * FROM {tableName}";

            ConfigureAdapter(query, out _dAdapt);

            DataTable table = new DataTable(tableName);
            try
            {
                _dAdapt.Fill(table);
                OnSuccess?.Invoke("Table fill Ok!");
            }
            catch (SqlException ex)
            {
                OnError?.Invoke(ex.ToString());
            }
            catch (InvalidOperationException ex)
            {
                OnError?.Invoke(ex.ToString());
            }
            return table;
        }

        //обновляем БД после изменений в dataGridView
        public void UpdateTable(DataTable table)
        {
            try
            {
                _dAdapt.Update(table);
            }
            catch (Exception ex)
            {
                OnError?.Invoke(ex.ToString());
            }
        }
    }
}
