﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnSql
{
    //Интерфейс для связи таблицы с БД
    interface ITableConnector
    {
        string[] GetTables();
        DataTable GetTableData(string tableName, string query = null);
        void UpdateTable(DataTable table);
    }
}
