﻿using System;
using System.Windows.Forms;

namespace LearnSql
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Sql());
        }
    }
}
