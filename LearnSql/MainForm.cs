﻿using System;
using System.Data;
using System.Windows.Forms;

namespace LearnSql
{
    public partial class Sql : Form
    {
        //получаем Sql коннектор и колоризатор для текста
        private readonly SqlConnector _connector = new SqlConnector();
        private readonly IColorize _colorize = new SqlColorizer();

        //указатель на метку для её последующего обновления через подписку на событие
        private static Label _statusLabel;

        public Sql()
        {
            InitializeComponent();
            _statusLabel = statusLabel;
            //подписываемся на события связанные с исключениями
            _connector.OnError += ShowMessage;
            //подписываемся на события связанные с нашими действиями
            _connector.OnSuccess += ShowStatus;
            //получаем список таблиц и заполняем comboTables
            comboTables.Items.AddRange(_connector.GetTables());
        }

        //определяем метод для показа перехваченных исключений из Sql коннектора
        private static void ShowMessage(string message) => MessageBox.Show(message);

        private static void ShowStatus(string message) => _statusLabel.Text = message;


        //отправляем команду, если в запросе не было SELECT, то через ExecuteNonQuery, иначе через SqlDataAdapter
        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(sqlCommandText.Text))
            {
                if (sqlCommandText.Text.ToUpper().Contains("SELECT"))
                    dataGridView.DataSource =
                        _connector.GetTableData(comboTables.SelectedItem.ToString(), sqlCommandText.Text);
                else _connector.DoQuery(sqlCommandText.Text);
            }
        }

        //при выборе таблицы из списка показывем её в dataGridView
        private void comboTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView.DataSource = _connector.GetTableData(comboTables.SelectedItem.ToString());
        }

        //при редактировании ячейки в dataGridView обновляем БД
        private void dataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView.EndEdit();
            _connector.UpdateTable((DataTable) dataGridView.DataSource);
        }

        //при нажатии на Enter во время редактирования ячейки сохраняем результат в БД
        private void dataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView.EndEdit();
            _connector.UpdateTable((DataTable) dataGridView.DataSource);
        }

        //при наборе текста подсвечиваем Sql синтаксис
        private void sqlCommandText_KeyPress(object sender, EventArgs e) => _colorize.Do(true, ref sqlCommandText);
    }
}
