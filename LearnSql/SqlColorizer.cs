﻿using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace LearnSql
{
    //раскрашиваем SQL синтаксис и переводим в верхний регистр если up = true
    public class SqlColorizer : IColorize
    {
        //зарезервированные SQL слова
        private static string SqlTokens = "(\\bADD\\b|\\bEXTERNAL\\b|\\bPROCEDURE\\b|\\bALL\\b|\\bFETCH\\b|\\bPUBLIC\\b|\\bALTER\\b|\\bFILE\\b|\\bRAISERROR\\b|\\bAND\\b|\\b" +
                                          "FILLFACTOR\\b|\\bREAD\\b|\\bANY\\b|\\bFOR\\b|\\bREADTEXT\\b|\\bAS\\b|\\bFOREIGN\\b|\\bRECONFIGURE\\b|\\bASC\\b|\\bFREETEXT\\b|\\b" +
                                          "REFERENCES\\b|\\bAUTHORIZATION\\b|\\bFREETEXTTABLE\\b|\\bREPLICATION\\b|\\bBACKUP\\b|\\bFROM\\b|\\bRESTORE\\b|\\b" +
                                          "BEGIN\\b|\\bFULL\\b|\\bRESTRICT\\b|\\bBETWEEN\\b|\\bFUNCTION\\b|\\bRETURN\\b|\\bBREAK\\b|\\bGOTO\\b|\\bREVERT\\b|\\bBROWSE\\b|\\b" +
                                          "GRANT\\b|\\bREVOKE\\b|\\bBULK\\b|\\bGROUP\\b|\\bRIGHT\\b|\\bBY\\b|\\bHAVING\\b|\\bROLLBACK\\b|\\bCASCADE\\b|\\bHOLDLOCK\\b|\\bROWCOUNT\\b|\\b" +
                                          "CASE\\b|\\bIDENTITY\\b|\\bROWGUIDCOL\\b|\\bCHECK\\b|\\bIDENTITY_INSERT\\b|\\bRULE\\b|\\bCHECKPOINT\\b|\\bIDENTITYCOL\\b|\\b" +
                                          "SAVE\\b|\\bCLOSE\\b|\\bIF\\b|\\bSCHEMA\\b|\\bCLUSTERED\\b|\\bIN\\b|\\bSECURITYAUDIT\\b|\\bCOALESCE\\b|\\bINDEX\\b|\\bSELECT\\b|\\bCOLLATE\\b|\\b" +
                                          "INNER\\b|\\bSEMANTICKEYPHRASETABLE\\b|\\bCOLUMN\\b|\\bINSERT\\b|\\bSEMANTICSIMILARITYDETAILSTABLE\\b|\\b" +
                                          "COMMIT\\b|\\bINTERSECT\\b|\\bSEMANTICSIMILARITYTABLE\\b|\\bCOMPUTE\\b|\\bINTO\\b|\\bSESSION_USER\\b|\\bCONSTRAINT\\b|\\b" +
                                          "IS\\b|\\bSET\\b|\\bCONTAINS\\b|\\bJOIN\\b|\\bSETUSER\\b|\\bCONTAINSTABLE\\b|\\bKEY\\b|\\bSHUTDOWN\\b|\\bCONTINUE\\b|\\bKILL\\b|\\bSOME\\b|\\b" +
                                          "CONVERT\\b|\\bLEFT\\b|\\bSTATISTICS\\b|\\bCREATE\\b|\\bLIKE\\b|\\bSYSTEM_USER\\b|\\bCROSS\\b|\\bLINENO\\b|\\bTABLE\\b|\\bCURRENT\\b|\\b" +
                                          "LOAD\\b|\\bTABLESAMPLE\\b|\\bCURRENT_DATE\\b|\\bMERGE\\b|\\bTEXTSIZE\\b|\\bCURRENT_TIME\\b|\\bNATIONAL\\b|\\bTHEN\\b|\\b" +
                                          "CURRENT_TIMESTAMP\\b|\\bNOCHECK\\b|\\bTO\\b|\\bCURRENT_USER\\b|\\bNONCLUSTERED\\b|\\bTOP\\b|\\bCURSOR\\b|\\bNOT\\b|\\bTRAN\\b|\\b" +
                                          "DATABASE\\b|\\bNULL\\b|\\bTRANSACTION\\b|\\bDBCC\\b|\\bNULLIF\\b|\\bTRIGGER\\b|\\bDEALLOCATE\\b|\\bOF\\b|\\bTRUNCATE\\b|\\bDECLARE\\b|\\b" +
                                          "OFF\\b|\\bTRY_CONVERT\\b|\\bDEFAULT\\b|\\bOFFSETS\\b|\\bTSEQUAL\\b|\\bDELETE\\b|\\bON\\b|\\bUNION\\b|\\bDENY\\b|\\bOPEN\\b|\\bUNIQUE\\b|\\bDESC\\b|\\b" +
                                          "OPENDATASOURCE\\b|\\bUNPIVOT\\b|\\bDISK\\b|\\bOPENQUERY\\b|\\bUPDATE\\b|\\bDISTINCT\\b|\\bOPENROWSET\\b|\\bUPDATETEXT\\b|\\b" +
                                          "DISTRIBUTED\\b|\\bOPENXML\\b|\\bUSE\\b|\\bDOUBLE\\b|\\bOPTION\\b|\\bUSER\\b|\\bDROP\\b|\\bOR\\b|\\bVALUES\\b|\\bDUMP\\b|\\bORDER\\b|\\bVARYING\\b|\\b" +
                                          "ELSE\\b|\\bOUTER\\b|\\bVIEW\\b|\\bEND\\b|\\bOVER\\b|\\bWAITFOR\\b|\\bERRLVL\\b|\\bPERCENT\\b|\\bWHEN\\b|\\bESCAPE\\b|\\bPIVOT\\b|\\bWHERE\\b|\\bEXCEPT\\b|\\b" +
                                          "PLAN\\b|\\b WHILE\\b|\\bEXEC\\b|\\bPRECISION\\b|\\bWITH\\b|\\bEXECUTE\\b|\\bPRIMARY\\b|\\bWITHIN GROUP\\b|\\bEXISTS\\b|\\bPRINT\\b|\\b" +
                                          "WRITETEXT\\b|\\bEXIT\\b|\\bPROC\\b)";
        public void Do(bool up, ref RichTextBox box)
        {

            Regex rex = new Regex(SqlTokens, RegexOptions.IgnoreCase);
            MatchCollection mc = rex.Matches(box.Text);
            int startCursorPosition = box.SelectionStart;
            box.SelectAll();   
            box.SelectionColor = Color.Black;
            if (up) box.SelectedText = box.SelectedText.ToLower();
            box.SelectionStart = startCursorPosition;
            foreach (Match m in mc)
            {
                int startIndex = m.Index;
                int stopIndex = m.Length;
                box.Select(startIndex, stopIndex);

                box.SelectionColor = Color.Blue;
                if(up) box.SelectedText = box.SelectedText.ToUpper();
                box.SelectionStart = startCursorPosition;
                box.SelectionColor = Color.Black;
            }
        }
    }
}
