﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace LearnSql
{
    //Интерфейс для соединения с БД
    interface IConnector
    {
        DbProviderFactory GetProvider(string dataProvider);
        void DoQuery(string query);
    }
}
